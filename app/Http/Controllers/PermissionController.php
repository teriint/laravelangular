<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use Validator;

class PermissionController extends Controller
{

    public function index(Request $request)
    {
        $permissions = Permission::get();
        return response()->json($permissions);
    }

    
    public function update(Request $request, $id = false)
    {
        $credentials = json_decode($request->getContent(), true);
        
        $rules = [
            'name' => 'required|max:255'
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $name = $credentials['name'];

            Permission::where('id', $id)->update(['name' => $name]);

            $permissionUpdate = Permission::get()->where('id', $id)->first() ;
            return response()->json(['success'=> true, 'message'=> 'The permission has been updated', 'data'=> [ 'permissions' => $permissionUpdate->name ] ]);
        
    }

    public function show(Request $request, $id)
    {
        $permissions = Permission::get()->where('id', $id)->first() ;
        return response()->json($permissions);
    }


    public function destroy($id)
    {
        $permission = Permission::find($id);
        $permission ->delete();
        return response()->json(['success'=> true, 'message'=> 'The permission was deleted' ]);
    }
}
