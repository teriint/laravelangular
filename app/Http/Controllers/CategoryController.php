<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::get();
        return response()->json($categories);
    }

    
    public function storeAndUpdate(Request $request, $id = false)
    {
        $credentials = json_decode($request->getContent(), true);
        
        $rules = [
            'name' => 'required|max:255'
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $name = $credentials['name'];
        
        if ($id==0) {
            $category = Category::create(['name' => $name]);
            return response()->json(['success'=> true, 'message'=> 'New category added' ]);
        } else {
            Category::where('id', $id)->update(['name' => $name]);
            return response()->json(['success'=> true, 'message'=> 'The note has been updated' ]);
        }
    }

    public function show(Request $request, $id)
    {
        $categories = Category::get()->where('id', $id)->first() ;
        return response()->json($notes);
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category ->delete();
        return response()->json(['success'=> true, 'message'=> 'The note was deleted' ]);
    }
}
