<?php

namespace App;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


use Tymon\JWTAuth\Contracts\JWTSubject;
class User extends Authenticatable
{
    use Notifiable;
    
    use EntrustUserTrait;

    protected $fillable = [
        'name', 'email', 'password', 'is_verified'
    ];

    
    protected $hidden = [
        'password', 'remember_token',
    ];

 

    // public function getJWTIdentifier()
    // {
    //     return $this->getKey();
    // }

    // public function getJWTCustomClaims()
    // {
    //     return [];
    // }

}
