<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Validator;

class RoleController extends Controller
{

    public function index(Request $request)
    {
        $roles = Role::get();
        return response()->json($roles);
    }

    
    public function update(Request $request, $id = false)
    {
        $credentials = json_decode($request->getContent(), true);
        
        $rules = [
            'name' => 'required|max:255'
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $name = $credentials['name'];

            $roleUpdate = Role::where('id', $id)->first();
            if ($roleUpdate->id != 1) {
                Role::where('id', $id)->update(['name' => $name]);
                return response()->json(['success'=> true, 'message'=> 'Имя роли было успешно изменено' ]);
            } else {
                return response()->json(['success'=> true, 'message'=> 'Наименование данной роли не может быть изменено']);
            }
    }

    public function show(Request $request, $id)
    {
        $roles = Role::get()->where('id', $id)->first() ;
        return response()->json($roles);
    }


    public function destroy($id)
    {
        $role = Role::find($id);
        $role ->delete();
        return response()->json(['success'=> true, 'message'=> 'The role was deleted' ]);
    }
}
