<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Note;
use App\File;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;

class NoteController extends Controller
{

    public function index()
    {
        $notes = Note ::get();
        $arrayNotes = json_decode(json_encode($notes), true);
        $reversNotes = array_reverse($arrayNotes);
        return response()->json($reversNotes);
        
    }

    public function storeAndUpdate(Request $request, $id = false)
    {
        $credentials = json_decode($request->getContent(), true);
        
        $rules = [
            'name' => 'required|max:255',
            'content' => 'required|max:255',
            'notes_category_id' => 'required|max:255'
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $name = $credentials['name'];
        $content = $credentials['content'];
        $notes_category_id =  $credentials['notes_category_id'];
        if ($id==0) {
            $note = Note::create(['name' => $name, 'content' => $content, 'notes_category_id' =>  $notes_category_id]);
            return response()->json(['success'=> true, 'message'=> 'New note added' ]);
        } else {
            Note::where('id', $id)->update(['name' => $name, 'content' => $content, 'notes_category_id' =>  $notes_category_id]);
            return response()->json(['success'=> true, 'message'=> 'The note has been updated' ]);
        }
    }

    public function show(Request $request, $id)
    {
        $notes = Note::get()->where('id', $id)->first();
        // $files = File::where('files_note_id', $notes->id)->first();
        return response()->json($notes);
    }



    public function destroy($id)
    {
        $note = Note::find($id);
        $note ->delete();
        return response()->json(['success'=> true, 'message'=> 'The note was deleted' ]);
    }
}
