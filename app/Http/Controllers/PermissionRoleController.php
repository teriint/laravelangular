<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PermissionRole;
use App\Role;
use App\User;
use Validator, Hash, Mail, DB;

class PermissionRoleController extends Controller
{
    public function index(Request $request)
    {
        $permission_roles = PermissionRole::get();

        return response()->json($permission_roles);
    }

    public function create(Request $request)
    {

        $credentials = json_decode($request->getContent(), true);

        $rules = [
            'permission_id' => 'required|max:255',
            'role_id' => 'required|max:255'
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $permission_id = $credentials['permission_id'];
        $role_id = $credentials['role_id'];  
        $i = count($permission_id);
        for ($i; $i != 0, $i--;) {
            $permission_role = PermissionRole::create( ['permission_id' => $permission_id[$i], 'role_id' => $role_id] );
        }
        return response()->json(['success'=> true, 'message'=> 'New permission_role added', $permission_role ]);
        
    }

    public function destroy($id)
    {
        $users = User::find($id);
        $role_user = DB::table('role_users')->where('user_id', '=', $id)->first()->delete();
        $permission_role = DB::table('permission_roles')->where('role_id', '=', $role_user->id)->first()->delete();
        $users->delete();
        $permission_role->delete();
        return response()->json(['success'=> true, 'message'=> 'The permission_role was deleted' ]);
    }
}
