<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoleUser;
use App\Role;
use App\User;
use Validator, Hash, Mail, DB;

class RoleUserController extends Controller
{

    public function index(Request $request)
    {
        $role_users = RoleUser::get();

        return response()->json($role_users);
    }

    public function create(Request $request)
    {

        $credentials = json_decode($request->getContent(), true);

        $rules = [
            'user_id' => 'required|max:255',
            'role_id' => 'required|max:255'
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $user_id = $credentials['user_id'];
        $role_id = $credentials['role_id'];  
       
        $role_user = RoleUser::create( ['role_id' => $role_id, 'user_id' => $user_id] );
        
        return response()->json(['success'=> true, 'message'=> 'New role_user added', $role_user ]);
        
    }

    public function destroy($id)
    {
        $users = User::find($id);
        $role_user = DB::table('role_users')->where('user_id', '=', $id)->first()->delete();
        $users->delete();
      
        $role_user->delete();
        return response()->json(['success'=> true, 'message'=> 'The role_user was deleted' ]);
    }
}
