<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\RoleUser;
use Illuminate\Http\Request;
use Validator, Hash, Mail, DB;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::get();
        $arrayUsers = json_decode(json_encode($users), true);
        $reversUsers = array_reverse($arrayUsers);
        return response()->json($reversUsers);
    }

    public function storeAndUpdate(Request $request, $id = false)
    {
        $credentials = json_decode($request->getContent(), true);
        
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|max:255'
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $name = $credentials['name'];
        $email = $credentials['email'];
        $password = $credentials['password'];
        $is_verified = 1;
        if ($id==0) {
            $user = User::create(['name' => $name, 'email' => $email, 'password' => Hash::make($password), 'is_verified' => $is_verified]);
            $verification_code = str_random(30); //Generate verification code
            DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);
            $subject = "Please verify your email address.";
            Mail::send('email.verify', ['name' => $name, 'verification_code' => $verification_code],
            function($mail) use ($email, $name, $subject){
                $mail->from(getenv('MAIL_FROM_ADDRESS'), "Service Notes");
                $mail->to($email, $name);
                $mail->subject($subject);
            });
            $getUser =  User::get()->where('id', $user->id)->first();
            $role_user = RoleUser::create(['user_id' => $getUser->id, 'role_id' => '3']);
            
            return response()->json(['success'=> true, 'message'=> 'New user added. Please check your email to complete your registration.', $role_user]);
        } else {
            
            User::where('id', $id)->update(['name' => $name, 'email' => $email , 'is_verified' => $is_verified]);
            $userUpdate = User::where('email', $request->email)->first();
            return response()->json(['success'=> true, 'message'=> 'The user has been updated', 'data'=> [ 'user_name' => $userUpdate->name ] ]);
        }
    }

    public function show(Request $request, $id)
    { 
        $users = User::get()->where('id', $id)->first();
        $role_user = RoleUser::get()->where('user_id', $users->id);
        $role = Role::get()->where('id', $role_user->role_id);
        return response()->json($users, $role_user, $role);
    }

    public function checkRole($id) {
        $role_user = DB::table('role_users')->where('user_id', $id)->first();
        return response()->json($role_user);
    }

    public function checkPermission($id) {
  
        $permission_role = DB::table('permission_roles')->where('permission_id', '=', $id)->first();
        return response()->json($permission_role);
    }

    public function destroy($id)
    {
 

        $user = User::where('id', $id)->first();
   
        $role_user = RoleUser::where('user_id', $user->id)->first();
        // $role_user->delete();
        $user->delete();
        
        return response()->json(['success'=> true, 'message'=> 'The note was deleted' ]);
    }
}
