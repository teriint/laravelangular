<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'name', 'content', 'notes_category_id'
    ];

    public function category() {
       return $this->belongsTo('App\Category', 'category_id');
    }
    
    public function file() {
        return $this->hasOne('App\File');
     }
}
