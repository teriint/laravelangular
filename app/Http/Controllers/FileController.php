<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Note;
use App\File;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;

class FileController extends Controller
{
    public function index()
    {
        $files = File::get();
        return response()->json($files);
        
    }

    public function show(Request $request, $id)
    {
        $notes = Note::get()->where('id', $id)->first();
        $files = File::get()->where('files_note_id', $notes->id)->first();
        return response()->json($files);
    }

    public function upload (Request $request) {
        
        $credentials = json_decode($request->getContent(), true);
    
        $name = $request->file->getClientOriginalName(); //получаем имя загружаемого файла
        $type = explode('.', $name);
            if ($type[1] == 'jpg' || $type[1] == 'png' || $type[1] == 'gif' || $type[1] == 'jpeg') {
            $file = $request->file; //присваиваем объект-файл переменной
            $file->move(public_path() . '\uploadfiles\ '.date('Y-m-d'), $name); // перемещаем файл в папку public/uploadfiles
            $path_3 = public_path($name).'\uploadfiles\ '.date('Y-m-d'); //получаем путь к папке
            
            $file_db = File::create(['name' => $name, 'path' => $path_3]); //записываем имя и путь к файлу в бд
            return response()->json([ 'success' => true,  $file_db ]);
            } else {
                return redirect('notes');
            }
    }
}
