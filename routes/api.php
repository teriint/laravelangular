<?php
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('recover', 'AuthController@recover');
Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'AuthController@logout');
});


Route::get('/notes', 'NoteController@index');

Route::group(['prefix' => 'notes'], function () {
    Route::post('/notes', 'NoteController@storeAndUpdate');
    Route::get('/notes/{note}', 'NoteController@show');
    Route::put('/notes/{note}', 'NoteController@storeAndUpdate');
    Route::delete('/notes/{note}', 'NoteController@destroy');
    Route::post('/notes/upload', 'NoteController@upload');
}
);

Route::get('/files', 'FileController@index');

Route::group(['prefix' => 'files'], function () {
    // Route::post('/notes', 'NoteController@storeAndUpdate');
     Route::get('/files/{file}', 'FileController@show');
    // Route::put('/notes/{note}', 'NoteController@storeAndUpdate');
    // Route::delete('/notes/{note}', 'NoteController@destroy');
    Route::post('/files/upload', 'FileController@upload');
}
);

Route::get('/category', 'CategoryController@index');

Route::group(['prefix' => 'category'], function () {
    Route::post('/category', 'CategoryController@storeAndUpdate');
    Route::get('/category/{category}', 'CategoryController@show');
    Route::put('/category/{category}', 'CategoryController@storeAndUpdate');
    Route::delete('/category/{category}', 'CategoryController@destroy');
}
);

Route::get('/user', 'UserController@index');

Route::group(['prefix' => 'user'], function () {
    Route::post('/user', 'UserController@storeAndUpdate');
    Route::get('/user/{user}', 'UserController@show');
    Route::put('/user/{user}', 'UserController@storeAndUpdate');
    Route::delete('/user/{user}', 'UserController@destroy');
    Route::get('/role/{user}', 'UserController@checkRole');
    Route::get('/permission/{user}', 'UserController@checkPermission');
}
);


Route::get('/role_user', 'RoleUserController@index');

Route::get('/permission_role', 'PermissionRoleController@index');

Route::post('/user_role/create', 'RoleUserController@create');

Route::post('/permission_role/create', 'PermissionRoleController@create');

Route::get('/role', 'RoleController@index');

Route::group(['prefix' => 'role'], function () {
    Route::post('role', 'JwtAuthenticateController@createRole');
    Route::get('/role/{role}', 'RoleController@show');
    Route::put('/role/{role}', 'RoleController@update');
    Route::delete('/role/{role}', 'RoleController@destroy');
}
);

Route::get('/permission', 'PermissionController@index');

Route::group(['prefix' => 'permission'], function () {
    Route::post('permission', 'JwtAuthenticateController@createPermission');
    Route::get('/permission/{permission}', 'PermissionController@show');
    Route::put('/permission/{permission}', 'PermissionController@update');
    Route::delete('/permission/{permission}', 'PermissionController@destroy');
}
);

// Route::post('role', 'JwtAuthenticateController@createRole');

// Route::post('permission', 'JwtAuthenticateController@createPermission');

Route::post('/assign_role', 'JwtAuthenticateController@assignRole');
Route::post('attachpermission', 'JwtAuthenticateController@attachPermission');
Route::group(['prefix' => 'api', 'middleware' => ['ability:admin,create-users']], function()
{
    Route::get('users', 'JwtAuthenticateController@index');
});

Route::post('authenticate', 'JwtAuthenticateController@authenticate');